from minio import Minio, S3Error
from flask import Flask, request, Response, jsonify
from bertopic import BERTopic
from bertopicllama import LlamaQuery
import mysql.connector
import numpy as np
import pandas as pd
import os
import io
import bertopic
import tempfile
import json
import re
import requests
from datetime import date

app = Flask(__name__)

# MinIO server details
MINIO_URL = "minio:9000" # Change to your MinIO URL
ACCESS_KEY = os.environ.get("MINIO_ACCESS_KEY")  # Change to your MinIO access key
SECRET_KEY = os.environ.get("MINIO_SECRET_KEY")  # Change to your MinIO secret key
MYSQL_ROOT_PASSWORD = os.environ.get("MYSQL_ROOT_PASSWORD")
BUCKET_NAME = "model-storage"

config = { # change this for prod.
    'user': 'root',
    'password': MYSQL_ROOT_PASSWORD,
    'host': 'vtmealplandb-mysql-headless',
    'database': 'vtmealplandb',
    'raise_on_warnings': True
}
# Initialize MinIO client
minioClient = Minio(MINIO_URL,
                    access_key=ACCESS_KEY,
                    secret_key=SECRET_KEY,
                    secure=False)  # Set to True for HTTPS
loaded_model = None
rep_docs = None
topic_themes = None

def remove_temp(temp_dir):
    '''
    Helper function to remove the temp_dir
    '''
    # Clean up temporary directory
    for filename in os.listdir(temp_dir):
        os.remove(os.path.join(temp_dir, filename))
    os.rmdir(temp_dir)

def load_model_from_minio(date):
    '''
    Loads the model form Minio
    '''
    global loaded_model
    global rep_docs
    global topic_themes
    # Create a temporary directory
    temp_dir = tempfile.mkdtemp()

    # List and download all objects from the model's folder in MinIO
    prefix = f"model-{date}/"
    try:
        for obj in minioClient.list_objects(BUCKET_NAME, prefix=prefix, recursive=True):
            # Download object into local directory
            obj_data = minioClient.get_object(BUCKET_NAME, obj.object_name)
            with open(os.path.join(temp_dir, obj.object_name.replace(prefix, '')), 'wb') as file_data:
                file_data.write(obj_data.read())
        

        file_path_rep = os.path.join(temp_dir, "rep_docs.json")

        with open(file_path_rep, "rb") as json_file:
            rep_docs = json.load(json_file)
        
        file_path_llm = os.path.join(temp_dir, "llm-topic-categories.json")

        with open(file_path_llm, "rb") as json_file:
            topic_themes = json.load(json_file)

        # Load BERTopic model
        loaded_model = BERTopic.load(temp_dir)
        remove_temp(temp_dir)
    
    except Exception as e:
        # Clean up temporary directory in case of failure
        remove_temp(temp_dir)
        
        return f"An error occurred: {e}", 400
    
    return loaded_model, rep_docs, topic_themes

def dev_load_model():
    '''
    Loads a dev model for testing.
    '''
    global loaded_model
    global rep_docs
    global topic_themes

    loaded_model = BERTopic.load("./model-04-14-2024")
    
    with open("./model-04-14-2024/rep_docs.json", "rb") as json_file:
        rep_docs = json.load(json_file)
    
    with open("./model-04-14-2024/llm-topic-categories.json", "rb") as json_file:
        topic_themes = json.load(json_file)
    

@app.route("/load-model/<date>")
def load_model(date):
    '''
    Wrapper function that loads the model data from a specific date.
    '''
    try:
        load_model_from_minio(date)
        return f"Model-{date} loaded successfully"
    except Exception as e:
        return f"An error occurred: {e}", 400

@app.route('/random-mealplan', methods=['GET'])
def meal_plan():
    global loaded_model
    global rep_docs

    if loaded_model is None:
        return jsonify({"error": "Model is not loaded"}), 400

    # Perform inference
    topics = loaded_model.get_topics()
    topic_nums = np.array(list(topics.keys()))
    random_clusters = np.random.choice(topic_nums, size=24).reshape(8, 3)

    rep_docs_values = np.array(list(rep_docs.values()))

    b_foods = []
    l_foods = []
    d_foods = []
    for cluster in random_clusters:
        b_cluster, l_cluster, d_cluster = cluster
        b_food = np.random.choice(rep_docs_values[b_cluster], size=1)[0]
        l_food = np.random.choice(rep_docs_values[l_cluster], size=1)[0]
        d_food = np.random.choice(rep_docs_values[d_cluster], size=1)[0]

        b_foods.append(b_food)
        l_foods.append(l_food)
        d_foods.append(d_food)

    
    return jsonify({"breakfast": b_foods, "lunch": l_foods, "dinner": d_foods})


@app.route('/mealplan/<userid>/<datequery>', methods=['GET'])
def smart_meal_plan(userid, datequery):
    '''
    This is the main functionality of our meal plan generation. Here
    are the steps that should be preformed in order:
    1. Make calls to the LLM to organize topic clusters into summaries using Q&A prompting. Example:
    [gelato, ice, flavored, cream, raspberry] should be defined as "Ice Cream" cluster.
    2. Once summaries are made, store in dictionary form where the topic id is the key, and the summary is the value.
    3. After sorting, retreive the fitness plan from the user: Cardio, bulking, etc.
    4. Use this personalized user meal plan to populate meal plans: Example: User A wants to bulk, use predefined meal-plan: {4 protein items, 3 salads, 2 sweets, etc}
    5. Gather a random subset of foods from the database and run predictions on the BERTopic model.
    6. After predictions are made, filter by the user predefined fitness-plan.
    7. That should be it.
    '''
    loaded_model, rep_docs, topic_themes = load_model_from_minio(datequery)
    # dev_load_model() # NOTE: Comment this out when not in development mode.
    # clean the output LLM data to be represented as one word responses.
    topic_themes = clean_llm_responses(topic_themes)

    # get the user's workout plan
    planid, workout_type, calorie_expect = get_user_workout_plan(userid)

    # serving time array, NOTE: there is no "Dinner" object in database??
    serving_times = ["Breakfast", "Lunch", "Dinner"]

    print("Topic Themes: ", topic_themes)
    print(f"\nWorkoutPlan: {workout_type}, calories expected: {calorie_expect}")

    serving_dfs = []

    for serving_time in serving_times:
        print(f"Serving time: {serving_time}")
        # get top-k random locations for a serving_time and date.
        food_locations = get_random_locations(num_locations=3, serving_time=serving_time, date_str=datequery)
        
        if len(food_locations) == 0:
            print(f"No {serving_time} options available!")
            continue

        # get foods by location, date, and serving_time
        foods_df = get_foods(food_locations[0], datequery, serving_time)

        # predict which topic the food item should be put into.
        preds = loaded_model.transform(foods_df["Food Name"])
        topic_preds = preds[0]

        # maps topics to themes
        mapped_themes = [topic_themes.get(str(pred), 'Unknown') for pred in topic_preds]
        
        # stores results in the food_df
        foods_df["Topics"] = mapped_themes
        # print(f"Food Topics: ", foods_df["Topics"])
        print(f"{foods_df.head()}")

        # define some refined dataframes for meal plan generation.
        protein_topics_df = foods_df[foods_df["Topics"] == "Protein"]
        protein_df = foods_df[foods_df["Protein"] >= 20]
        high_calorie_df = foods_df[foods_df["Calories"] >= 500]
        fast_food_df = foods_df[foods_df["Topics"] == "Fast Food"]
        drinks_df = foods_df[foods_df["Topics"] == "Drink"]
        seafood_df = foods_df[foods_df["Topics"] == "Seafood"]

        # HERE define custom mealplans
        result_df = pd.DataFrame(columns=foods_df.columns)

        cardio_plan = {
            'Protein Topics': {'df': protein_topics_df, 'samples': 1},
            'Protein': {'df': protein_df, 'samples': 1},
            'High Calorie': {'df': high_calorie_df, 'samples': 2},
            'Drinks': {'df': drinks_df, 'samples': 1}
        }

        bulk_plan = {
            'Protein Topics': {'df': protein_topics_df, 'samples': 1},
            'Protein': {'df': protein_df, 'samples': 2},
            'High Calorie': {'df': high_calorie_df, 'samples': 2},
            'Drinks': {'df': drinks_df, 'samples': 1}
        }

        aerobics_plan = {
            'High Calorie': {'df': high_calorie_df, 'samples': 1},
            'Drinks': {'df': drinks_df, 'samples': 1}
        }

        default_plan = {
            'Fast Food': {'df': fast_food_df, 'samples': 2},
            'High Calorie': {'df': high_calorie_df, 'samples': 1},  
            'Drinks': {'df': drinks_df, 'samples': 1}
        }

        if workout_type == "Cardio":
            result_df = sample_plan(result_df, cardio_plan, serving_time)

        elif workout_type == "Bulk":
            result_df = sample_plan(result_df, bulk_plan, serving_time)

        elif workout_type == "Aerobics":
            result_df = sample_plan(result_df, aerobics_plan, serving_time)

        else:
            result_df = sample_plan(result_df, default_plan, serving_time)
     
        serving_dfs.append(result_df)
        
    return jsonify({"breakfast": serving_dfs[0].to_dict(), "lunch": serving_dfs[1].to_dict(), "dinner": serving_dfs[2].to_dict()})

def sample_plan(result_df, plan, serving_time):
    '''
    Samples the plan specified, checks for any sampling errors, and returns a result dataframe.
    '''
    for category, details in plan.items():
        if not details['df'].empty:
            # Handle cases where the DataFrame may not have the desired number of samples
            n_samples = min(details['samples'], len(details['df']))
            sampled_items = details['df'].sample(n=n_samples)
            result_df = pd.concat([result_df, sampled_items], ignore_index=True)
        else:
            print(f"For {serving_time}, \"{category}\" dataframe has 0 entries, skipping in the meal plan...")

    return result_df

def clean_llm_responses(data):
    '''
    Given noisy LLM response data, clean it up so it just outputs a dictionary with topics as keys
    and the answer corresponding to the letter choice as values.
    '''
    cleaned_dict = {}
    lq = LlamaQuery()
    topic_theme_choices = lq.get_options()

    for key, value in data.items():
        if value is None:
            cleaned_dict[key] = 'Unknown'
        else:
            match = re.search(r'\b([A-G])\b', value)
            if match:
                cleaned_dict[key] = match.group(1)
                cleaned_dict[key] = topic_theme_choices[cleaned_dict[key]]
            else:
                cleaned_dict[key] = 'Unknown'

    return cleaned_dict

def get_user_workout_plan(userid):
    '''
    SELECT WorkoutPlan.workoutPlanID, WorkoutPlan.workoutType, WorkoutPlan.workoutCalorieExpect
    FROM WorkoutPlan wp
    JOIN FitnessStat fs ON wp.workoutPlanID = fs.workoutPlanID
    WHERE fs.userID = ?
    '''
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    query = """
    SELECT wp.workoutPlanID, wp.workoutType, wp.workoutCalorieExpect
    FROM WorkoutPlan wp
    JOIN FitnessStat fs ON wp.workoutPlanID = fs.workoutPlanID
    WHERE fs.userID = %s
    """

    cursor.execute(query, (userid,))

    # Fetch all the rows
    results = cursor.fetchall()

    for row in results:
        planID = row[0]
        workoutType = row[1]
        calorieExpectation = row[2]

    cursor.close()
    cnx.close()

    return planID, workoutType, calorieExpectation

def get_random_locations(num_locations, serving_time, date_str):
    '''
    SELECT Food.foodLocation 
    FROM Food JOIN FoodDate 
    ON Food.foodID = FoodDate.foodID 
    WHERE Food.foodServingTime = %s AND FoodDate.foodDateDate = %s 
    ORDER BY RAND() LIMIT %s
    '''
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    query = """
    SELECT Food.foodLocation 
    FROM Food JOIN FoodDate 
    ON Food.foodID = FoodDate.foodID 
    WHERE Food.foodServingTime = %s AND FoodDate.foodDateDate = %s 
    ORDER BY RAND() LIMIT %s
    """

    cursor.execute(query, (serving_time, date_str, num_locations))

    query_results = cursor.fetchall()
    locations = []

    for row in query_results:
        locations.append(row[0])

    cursor.close()
    cnx.close()
    return locations

def get_foods(location, date_str, serving_time):
    '''
    SELECT Food.foodName, ...,
    Food.foodLocation,
    FoodDate.foodDateDate
    FROM 
        Food
    JOIN 
        FoodDate ON Food.foodID = FoodDate.foodID
    WHERE 
        Food.foodLocation = %s AND FoodDate.foodDateDate = %s
    ORDER BY 
        FoodDate.foodDateDate;
    '''  
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    date_arr = date_str.split("-")
    year, month, day = int(date_arr[0]), int(date_arr[1]), int(date_arr[2])
    date_query = date(year, month, day)
    
    query = """
    SELECT
        F.foodID, 
        F.foodName,
        F.foodCalorie,
        F.foodFat,
        F.foodCholesterol,
        F.foodSodium,
        F.foodCarb,
        F.foodSugar,
        F.foodProtein,
        F.foodServingTime,
        F.foodLocation,
        FD.foodDateDate
    FROM 
        Food F
    JOIN 
        FoodDate FD ON F.foodID = FD.foodID
    WHERE 
        F.foodLocation = %s AND FD.foodDateDate = %s AND F.foodServingTime = %s
    ORDER BY 
        FD.foodDateDate;
    """

    cursor.execute(query, (location, date_query, serving_time))
    results = cursor.fetchall()
    # for row in results:
    #     print(row)
    df = pd.DataFrame(results, columns=[
    "foodID", "Food Name", "Calories", "Fat", "Cholesterol", "Sodium", "Carbohydrates",
    "Sugar", "Protein", "Serving Time", "Location", "Date"
    ])

    cursor.close()
    cnx.close()
    return df

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)