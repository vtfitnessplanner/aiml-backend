# AIML-Backend

## Introduction
This code runs inference on the BERTopic models stored in the minio instance.

## Example Working Routes on Cluster

#### Load the Model into Backend
This request loads a model saved in the Minio instance by its date.

```
curl http://gobbleup-aiml-backend-svc:5000/load-model/2024-04-01
```

#### Generate a Random Meal Plan
NOTE: make sure the `load-model` route has been called before running this.

This request sends a JSON file of the generated random meal plan.

```
curl -X post http://gobbleup-aiml-backend-svc:5000/random-mealplan
```

output example:
```
{
  "breakfast": [
    "Soy Cafe Aulait, Short",
    "Marinara Sauce",
    "Iced Caramel Macchiato, Tall",
    "Soy Iced Caramel Macchiato, Short",
    "Grilled Cheese Sandwich",
    "Boneless Buffalo Wings",
    "Boneless Buffalo Wings",
    "Bagels, Assorted"
  ],
  "dinner": [
    "Golden Raisins",
    "Soy Iced Flavored Latte, Grande",
    "GNG Vegan Blueberry Muffin",
    "Salad Dressing, Ranch",
    "Fresh Red Delicious Apple",
    "Chocolate Caramel Latte Gelato",
    "Cheese Burger",
    "Marinara Sauce"
  ],
  "lunch": [
    "Boneless Buffalo Wings",
    "Chik'n Ranch Wrap",
    "Salad Dressing, Ranch",
    "Bursting Strawberry Boba",
    "Bursting Strawberry Boba",
    "Spinach and Feta Bistro Pastry",
    "Salad Dressing, Caesar",
    "Sugar Free Caramel Syrup"
  ]
}
```