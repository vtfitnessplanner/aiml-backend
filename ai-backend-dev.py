from minio import Minio, S3Error
from flask import Flask, request, Response, jsonify
from bertopic import BERTopic
import numpy as np
import os
import io
import bertopic
import tempfile
import json

app = Flask(__name__)

# MinIO server details
MINIO_URL = "localhost:9000" # Change to your MinIO URL
ACCESS_KEY = "grizzlord-dev"  # Change to your MinIO access key
SECRET_KEY = "grizzlord-dev-password"  # Change to your MinIO secret key
BUCKET_NAME = "model-storage"

# Initialize MinIO client
minioClient = Minio(MINIO_URL,
                    access_key=ACCESS_KEY,
                    secret_key=SECRET_KEY,
                    secure=False)  # Set to True for HTTPS
loaded_model = None
rep_docs = None

def load_model_from_minio(date):
    '''
    Loads the model form Minio
    '''
    global loaded_model
    global rep_docs
    # Create a temporary directory
    temp_dir = tempfile.mkdtemp()

    # List and download all objects from the model's folder in MinIO
    prefix = f"model-{date}/"
    try:
        for obj in minioClient.list_objects(BUCKET_NAME, prefix=prefix, recursive=True):
            # Download object into local directory
            obj_data = minioClient.get_object(BUCKET_NAME, obj.object_name)
            with open(os.path.join(temp_dir, obj.object_name.replace(prefix, '')), 'wb') as file_data:
                file_data.write(obj_data.read())
        

        file_path = os.path.join(temp_dir, "rep_docs.json")

        if os.path.isfile(file_path):
            print(file_path)
            with open(file_path, "rb") as json_file:
                rep_docs = json.load(json_file)


        # Load BERTopic model
        loaded_model = BERTopic.load(temp_dir)

        
        # Clean up temporary directory
        for filename in os.listdir(temp_dir):
            os.remove(os.path.join(temp_dir, filename))
        os.rmdir(temp_dir)
    
    except Exception as e:
        # Clean up temporary directory in case of failure
        for filename in os.listdir(temp_dir):
            os.remove(os.path.join(temp_dir, filename))
        os.rmdir(temp_dir)
        
        return f"An error occurred: {e}", 400

@app.route("/load-model/<date>")
def load_model(date):
    '''
    Wrapper function that loads the model data from a specific date.
    '''
    try:
        load_model_from_minio(date)
        return f"Model-{date} loaded successfully"
    except Exception as e:
        return f"An error occurred: {e}", 400

@app.route('/mealplan', methods=['POST'])
def meal_plan():
    global loaded_model
    global rep_docs

    if loaded_model is None:
        return jsonify({"error": "Model is not loaded"}), 400

    # Perform inference
    topics = loaded_model.get_topics()
    topic_nums = np.array(list(topics.keys()))
    random_clusters = np.random.choice(topic_nums, size=24).reshape(8, 3)

    rep_docs_values = np.array(list(rep_docs.values()))

    b_foods = []
    l_foods = []
    d_foods = []
    for cluster in random_clusters:
        b_cluster, l_cluster, d_cluster = cluster
        b_food = np.random.choice(rep_docs_values[b_cluster], size=1)[0]
        l_food = np.random.choice(rep_docs_values[l_cluster], size=1)[0]
        d_food = np.random.choice(rep_docs_values[d_cluster], size=1)[0]

        b_foods.append(b_food)
        l_foods.append(l_food)
        d_foods.append(d_food)

    
    return jsonify({"breakfast": b_foods, "lunch": l_foods, "dinner": d_foods})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)